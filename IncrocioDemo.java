package lez_2016_10_31;

class IncrocioDemo {
    public static void main(String args[]){
        Incrocio i=new Incrocio();

        Macchina m1=new Macchina(i,1);
        Macchina m2=new Macchina(i,2);
        Macchina m3=new Macchina(i,3);
        Macchina m4=new Macchina(i,4);

        GUI g=new GUI(m1,m2,m3,m4,i);

        m1.start();
        m2.start();
        m3.start();
        m4.start();
    }
}
