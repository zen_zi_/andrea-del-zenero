package database;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUI_ALUNNO extends JFrame {

	private JPanel contentPane;
	private JTextField txtInserire;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI_ALUNNO frame = new GUI_ALUNNO();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI_ALUNNO() {
		setTitle("Inserimento Alunno");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtInserire = new JTextField();
		txtInserire.setToolTipText("Inserire");
		txtInserire.setBounds(167, 11, 185, 20);
		contentPane.add(txtInserire);
		txtInserire.setColumns(10);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(167, 42, 185, 20);
		contentPane.add(textField);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(167, 73, 185, 20);
		contentPane.add(textField_1);
		
		final JComboBox comboBox = new JComboBox();
		comboBox.setBounds(167, 104, 55, 20);
		contentPane.add(comboBox);
		comboBox.addItem("1");
		comboBox.addItem("2");
		comboBox.addItem("3");
		comboBox.addItem("4");
		comboBox.addItem("5");
		comboBox.addItem("6");
		comboBox.addItem("7");
		comboBox.addItem("8");
		comboBox.addItem("9");
		comboBox.addItem("10");
		comboBox.addItem("11");
		comboBox.addItem("12");
		comboBox.addItem("13");
		comboBox.addItem("14");
		comboBox.addItem("15");
		comboBox.addItem("16");
		comboBox.addItem("17");
		comboBox.addItem("18");
		comboBox.addItem("19");
		comboBox.addItem("20");
		comboBox.addItem("21");
		comboBox.addItem("22");
		comboBox.addItem("23");
		comboBox.addItem("24");
		comboBox.addItem("25");
		comboBox.addItem("26");
		comboBox.addItem("27");
		comboBox.addItem("28");
		comboBox.addItem("29");
		comboBox.addItem("30");
		comboBox.addItem("31");
		
		final JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(232, 104, 55, 20);
		contentPane.add(comboBox_1);
		comboBox_1.addItem("Gennaio");
		comboBox_1.addItem("Febbraio");
		comboBox_1.addItem("Marzo");
		comboBox_1.addItem("Aprile");
		comboBox_1.addItem("Maggio");
		comboBox_1.addItem("Giugno");
		comboBox_1.addItem("Luglio");
		comboBox_1.addItem("Agosto");
		comboBox_1.addItem("Settembre");
		comboBox_1.addItem("Ottobre");
		comboBox_1.addItem("Novembre");
		comboBox_1.addItem("Dicembre");
		
	    final JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setBounds(297, 104, 55, 20);
		contentPane.add(comboBox_2);
		comboBox_2.addItem("1994");
		comboBox_2.addItem("1995");
		comboBox_2.addItem("1996");
		comboBox_2.addItem("1997");
		comboBox_2.addItem("1998");
		comboBox_2.addItem("1999");
		comboBox_2.addItem("2000");
		comboBox_2.addItem("2001");
		comboBox_2.addItem("2002");
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(10, 14, 46, 14);
		contentPane.add(lblNome);
		
		JLabel lblCognome = new JLabel("Cognome");
		lblCognome.setBounds(10, 45, 46, 14);
		contentPane.add(lblCognome);
		
		JLabel lblIndirizzo = new JLabel("Indirizzo");
		lblIndirizzo.setBounds(10, 76, 46, 14);
		contentPane.add(lblIndirizzo);
		
		JLabel lblDataDiNascita = new JLabel("Data di nascita");
		lblDataDiNascita.setBounds(10, 107, 71, 14);
		contentPane.add(lblDataDiNascita);
		
		JButton btnSalva = new JButton("Salva");
		btnSalva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String giorno = (String) comboBox.getSelectedItem();
				String mese = (String) comboBox_1.getSelectedItem();
				String anno = (String) comboBox_2.getSelectedItem();
			
			}
		});
		btnSalva.setVerticalAlignment(SwingConstants.TOP);
		btnSalva.setBounds(153, 198, 89, 23);
		contentPane.add(btnSalva);
	}
}
