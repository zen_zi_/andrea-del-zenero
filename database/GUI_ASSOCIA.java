package database;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUI_ASSOCIA extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI_ASSOCIA frame = new GUI_ASSOCIA();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI_ASSOCIA() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAlunno = new JLabel("Alunno");
		lblAlunno.setBounds(10, 35, 46, 14);
		contentPane.add(lblAlunno);
		
		JLabel lblClasse = new JLabel("Classe");
		lblClasse.setBounds(10, 160, 46, 14);
		contentPane.add(lblClasse);
		
		final JComboBox comboBox = new JComboBox();
		comboBox.setBounds(173, 32, 186, 20);
		contentPane.add(comboBox);
		
		final JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(173, 157, 186, 20);
		contentPane.add(comboBox_1);
		
		JButton btnSalva = new JButton("Salva");
		btnSalva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String alunno = (String) comboBox.getSelectedItem();
				String classe = (String) comboBox_1.getSelectedItem();
			}
		});
		btnSalva.setBounds(152, 209, 89, 23);
		contentPane.add(btnSalva);
	}

}
