package database;

//STEP 1. Import required packages
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class FirstExample {
static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
static final String DB_URL = "jdbc:mysql://localhost/scuola";

 static final String USER = "root";
 static final String PASS = "";

 public static void main(String[] args) {
     Connection conn = null;
     Statement stmt = null;

 try{
       Class.forName("com.mysql.jdbc.Driver");


    System.out.println("Connecting to database...");
    conn = DriverManager.getConnection(DB_URL,USER,PASS);

    System.out.println("Creating statement...");
    stmt = conn.createStatement();
    int id = 2;
    String c = "C";
    String info = "informatica";
    String sql = "INSERT INTO classe (id, sigla, anno) VALUES ("+id+", '"+c+"', '"+info+"');";
    stmt.executeQuery(sql);
    stmt.close();
    conn.close();
 }catch(SQLException se){
    se.printStackTrace();
 }catch(Exception e){
    e.printStackTrace();
 }finally{
    try{
           if(stmt!=null)
              stmt.close();
    }catch(SQLException se2){

    }
    try{
        if(conn!=null)
                 conn.close();
    }catch(SQLException se){
              se.printStackTrace();
    }
 }
 System.out.println("Goodbye!");
}
}