package database;

import java.util.*;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JComboBox;
import javax.swing.JLabel;

public class GUI_A_S_I extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI_A_S_I frame = new GUI_A_S_I();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI_A_S_I() {
		setTitle("Inserimento Classe");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		final JComboBox comboBox = new JComboBox();
		comboBox.setBounds(238, 11, 114, 20);
		contentPane.add(comboBox);
		comboBox.addItem("1a");
		comboBox.addItem("2a");
		comboBox.addItem("3a");
		comboBox.addItem("4a");
		comboBox.addItem("5a");
		
		final JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(238, 72, 114, 20);
		contentPane.add(comboBox_1);
		comboBox_1.addItem("A");
		comboBox_1.addItem("B");
		comboBox_1.addItem("C");
		comboBox_1.addItem("D");
		comboBox_1.addItem("E");
		
		final JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setBounds(238, 142, 114, 20);
		contentPane.add(comboBox_2);
		comboBox_2.addItem("Biennio");
		comboBox_2.addItem("Informatica");
		comboBox_2.addItem("Edilizia");
		comboBox_2.addItem("Elettronica");
		comboBox_2.addItem("Elettrotecnica");
		comboBox_2.addItem("Occhiale");
		comboBox_2.addItem("Meccanica");
		
		
		JButton btnNewButton = new JButton("Salva");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String classe = (String) comboBox.getSelectedItem();
				String sezione = (String) comboBox_1.getSelectedItem();
				String indirizzo = (String) comboBox_2.getSelectedItem();
			}
			
		});
		
		
		btnNewButton.setBounds(172, 201, 95, 23);
		contentPane.add(btnNewButton);
		
		
		JLabel lblAnno = new JLabel("Anno");
		lblAnno.setBounds(10, 14, 46, 14);
		contentPane.add(lblAnno);
		
		JLabel lblSezione = new JLabel("Sezione");
		lblSezione.setBounds(10, 75, 46, 14);
		contentPane.add(lblSezione);
		
		JLabel lblIndirizzoScolastico = new JLabel("Indirizzo scolastico");
		lblIndirizzoScolastico.setBounds(10, 145, 144, 14);
		contentPane.add(lblIndirizzoScolastico);
		
		
	}
	
}
