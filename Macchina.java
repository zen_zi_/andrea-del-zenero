package lez_2016_10_31;

class Macchina extends Thread{
    int strada;
    int DESTRA=1,DRITTO=2,SINISTRA=3;
    int scelta,dist;
    Incrocio i;

    Macchina(Incrocio i, int n){
        this.i=i;
        strada=n;
    }

    public int getDist() {
        return dist;
    }

    @Override
    public void run() {
        super.run();
        while(dist!=100){
            try{
                Thread.sleep(50);
            }catch (InterruptedException w){
                w.printStackTrace();
            }
            dist+=10;
            if(dist==50) {
                scelta=(int)(Math.random()*4);
                if(scelta==DESTRA) {
                    strada=i.svoltaDestra();
                }
                else if(scelta==DRITTO) {
                    strada=i.dritto();
                }
                else {
                    strada=i.svoltaSinistra();
                }
            }
        }
    }
}
